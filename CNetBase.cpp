#include "CNetBase.hpp"

#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include <QDebug>

#include <QTime>
#include <QCoreApplication>
#include <QEvent>
#include <QFile>

const QString CNetBase::serverIdentifier = "UntitledServer";
const QString CNetBase::clientIdentifier = "UntitledClient";
QEvent::Type CNetBase::EventTypeSendPackageRequest = QEvent::None;

/* Serverside peer */
CNetBase::CNetBase(QTcpSocket *socket, quint32 protocolVersion, QObject *parent) :
    QObject(parent),
    m_isConnected(false),
    m_expectedBytes(0),
    m_incomingBuffer(new CExtendedArray()),
    m_outgoingBuffer(new CExtendedArray()),
    m_protocolVersion(protocolVersion)
{
    if (EventTypeSendPackageRequest == QEvent::None)
        EventTypeSendPackageRequest = static_cast<QEvent::Type>(QEvent::registerEventType());

    m_socket        = socket;
    m_mode          = Server;
    m_sendPackagePending = false;
    m_dumpPackage = false;
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(whenError()));

//    connect(this, SIGNAL(sendLog(QString)), this, SLOT(debugSink(QString)));

    protocolConnection();
}

/* Client */
CNetBase::CNetBase(quint32 protocolVersion, QObject *parent, QString host, quint16 port) :
    QObject(parent),
    m_isConnected(false),
    m_incomingBuffer(new CExtendedArray()),
    m_outgoingBuffer(new CExtendedArray()),
    m_protocolVersion(protocolVersion)
{
    if (EventTypeSendPackageRequest == QEvent::None)
        EventTypeSendPackageRequest = static_cast<QEvent::Type>(QEvent::registerEventType());

    m_socket = new QTcpSocket(this);
    m_mode   = Client;
    m_isConnected = false;
    m_dumpPackage = false;
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(whenError()));
    connect(m_socket, SIGNAL(connected()), SLOT(protocolConnection()));

    if ((port > 0) && !host.isEmpty())
        doConnect(host, port);
}

CNetBase::~CNetBase()
{
    if (m_socket->isValid())
        doDisconnect("Subsystem destroyed");

    delete m_incomingBuffer;
    delete m_outgoingBuffer;
}

void CNetBase::doConnect(QString host, quint16 port)
{
    if (m_isConnected)
        return;

    qDebug() << "New connect to" << host << "|" << port;
    m_socket->connectToHost(host, port);
}

void CNetBase::doDisconnect(QString reason)
{
    if (reason.isEmpty())
        reason = QLatin1String("No reason given");

    if (!m_socket->isValid())
        return;

    qDebug() << Q_FUNC_INFO;
    sendPackage(); // Send accumulated data

    CExtendedArray goodbyePackage;
    goodbyePackage.writeQUInt32(0);
    goodbyePackage.writeQString(reason);
    m_socket->write(goodbyePackage.constData(), goodbyePackage.size());
    m_socket->close();
}

void CNetBase::whenSocketDisconnected()
{
    if (!m_isConnected)
        return;

    qDebug() << Q_FUNC_INFO;
    disconnect(m_socket, SIGNAL(readyRead()), this, SLOT(protocolAcception()));
    disconnect(m_socket, SIGNAL(readyRead()), this, SLOT(acceptData()));
    disconnect(m_socket, SIGNAL(disconnected()), this, SLOT(whenSocketDisconnected()));

    m_isConnected = false;
    emit disconnected();
}

void CNetBase::whenError()
{
    qDebug() << "Error: " << m_socket->errorString();
}

bool CNetBase::isConnected()
{
    return m_isConnected;
}

void CNetBase::protocolConnection()
{
    CExtendedArray serverPackage;
    serverPackage.writeQUInt32(serverIdentifier.size());
    serverPackage.writeBytes(serverIdentifier.toLocal8Bit().constData(), serverIdentifier.size());
    serverPackage.writeQUInt32(m_protocolVersion);

    CExtendedArray clientPackage;
    clientPackage.writeQUInt32(clientIdentifier.size());
    clientPackage.writeBytes(clientIdentifier.toLocal8Bit().constData(), clientIdentifier.size());
    clientPackage.writeQUInt32(m_protocolVersion);

    if (m_mode == Server) {
        m_socket->write(serverPackage.constData(), serverPackage.size());
        m_protocolConnectionBytes = clientPackage.size();
    }
    else /*if (m_mode == Client)*/ {
        m_socket->write(clientPackage.constData(), clientPackage.size());
        m_protocolConnectionBytes = serverPackage.size();
    }

    if (m_socket->bytesAvailable() > 0)
        protocolAcception();

    connect(m_socket, SIGNAL(readyRead()), SLOT(protocolAcception()));
    connect(m_socket, SIGNAL(disconnected()), SLOT(whenSocketDisconnected()));
}

void CNetBase::protocolAcception()
{
    if (m_isConnected)
        return; // Extra call?

    if (m_socket->bytesAvailable() < m_protocolConnectionBytes) {
        qDebug() << "Not enough bytes to be complete identifier package. Waiting.";
        return;
    }

    disconnect(m_socket, SIGNAL(readyRead()), this, SLOT(protocolAcception()));

    sendLog("Test Identify.");

    CExtendedArray connectionData;
    connectionData.append(m_socket->read(m_protocolConnectionBytes));

    int remoteIdentifierSize     = connectionData.readQUInt32();
    QString remoteIdentifier     = connectionData.readBytes(remoteIdentifierSize);
    quint8 remoteProtocolVersion = connectionData.readQUInt32();

    if (((m_mode == Server) && (remoteIdentifier != clientIdentifier)) ||
            ((m_mode == Client) && (remoteIdentifier != serverIdentifier))) {
        qDebug() << "Remote identifier isn't correct.";
        return;
    }

    if (remoteProtocolVersion != m_protocolVersion) {
        qDebug() << "Protocol version is incompatible.";
        return;
    }

    m_isConnected = true;
    m_expectedBytes = 0;
    m_sendPackagePending = false;

    connect(m_socket, SIGNAL(readyRead()), SLOT(acceptData()));

    emit connected();

    acceptData();
}

void CNetBase::acceptData()
{
    if (readPackage())
        emit processPackage();
}

void CNetBase::sendPackage(bool force)
{
    if (!(m_sendPackagePending || force))
        return;

    m_sendPackagePending = false;
    quint32 size;
    size = m_outgoingBuffer->size();
    if (size > 0) {
//        emit sendLog("Send package.");
        m_socket->write((const char *) &size, 4);
//        qDebug() << "Send time:" << QTime::currentTime().msec();

        m_socket->write(m_outgoingBuffer->constData(), m_outgoingBuffer->size());
//        m_socket->waitForBytesWritten(1000);
        m_outgoingBuffer->reset();
        qDebug() << QString("Sended package (%1 bytes)").arg(size);
    }
}

QString CNetBase::socketAddress()
{
    if (m_mode == Client)
        return QString("%1:%2").arg(m_socket->localAddress().toString()).arg(m_socket->localPort());
    else
        return QString("%1:%2").arg(m_socket->peerAddress().toString()).arg(m_socket->peerPort());
}

bool CNetBase::atEnd()
{
    if (m_incomingBuffer->atEnd())
        return !readPackage();

    return false;
}

void CNetBase::enableDebug(bool enabled)
{
    m_dumpPackage = enabled;
}

void CNetBase::setDumpFilename(const QString &filename)
{
    m_dumpFilename = filename;
}

bool CNetBase::event(QEvent *event)
{
    if (event->type() == EventTypeSendPackageRequest) {
        sendPackage();
        event->accept();
        return true;
    }
    return QObject::event(event);
}

void CNetBase::debugSink(QString text)
{
    qDebug() << QString("NetDebug: %1").arg(text);
}

void CNetBase::requestSendPackage()
{
    if (m_sendPackagePending)
        return;

    m_sendPackagePending = true;
    QCoreApplication::postEvent(this, new QEvent(EventTypeSendPackageRequest));
}

bool CNetBase::readPackage()
{
    //    qDebug() << "acceptData time:" << QTime::currentTime().msec();
    if (m_expectedBytes == 0)
    {
        if (m_socket->bytesAvailable() < 4)
            return false;

        m_incomingBuffer->reset();
        emit sendLog("New package\n");

        m_incomingBuffer->append(m_socket->read(4));
        m_expectedBytes = readQUInt32();
        /* Since we know size, let's reset buffer and fill it with usefull data only */
        m_incomingBuffer->reset();
    }
    else if (m_socket->bytesAvailable() < 1)
        return false;

//    emit sendLog(QString("Received bytes: %1\n").arg(m_socket->bytesAvailable()));
//    qDebug() << QString("Received bytes: %1\n").arg(m_socket->bytesAvailable());

    m_incomingBuffer->append(m_socket->read(m_expectedBytes - m_incomingBuffer->size()));

    if (m_expectedBytes == 0) { // If readed package have 0 expected bytes in header, it's disconnect package
        emit sendLog(QLatin1String("Disconnect package accepted."));
        m_socket->close();
        return false;
    }

    if (m_expectedBytes > m_incomingBuffer->size())
    {
        emit sendLog(QString("Wait for new pieces: %1/%2 bytes readed").arg(m_incomingBuffer->size()).arg(m_expectedBytes));
        return false; // Wait for new pieces
    }

    /* Package received */
    emit sendLog(QString("Package fully received: %1/%2 bytes readed").arg(m_incomingBuffer->size()).arg(m_expectedBytes));

    m_expectedBytes = 0;

    if (m_dumpPackage && !m_dumpFilename.isEmpty()) {
        QFile packageDump(m_dumpFilename);
        packageDump.open(QIODevice::WriteOnly);
        packageDump.write(*m_incomingBuffer);
        m_incomingBuffer->toBegin();
    }

    return true;
}
