#ifndef CNETBASE_H
#define CNETBASE_H

#include <QObject>
#include <QEvent>

#include "CExtendedArray.hpp"

class QTcpSocket;

class CNetBase : public QObject
{
    Q_OBJECT
public:
    enum NetRole {
        Client,
        Server
    };

    explicit CNetBase(QTcpSocket *socket, quint32 protocolVersion, QObject *parent = 0);
    explicit CNetBase(quint32 protocolVersion, QObject *parent = 0, QString host = QString(), quint16 port = 0);

    ~CNetBase();

    void doConnect(QString host, quint16 port);
    void doDisconnect(QString reason);

    /* isConnected() mean that not just tcp, but game-protocol connection is established. */
    bool isConnected();

    QString socketAddress();

    bool atEnd();

    inline quint8     readNetCommand() { return m_incomingBuffer->readQUInt8(); }
    inline quint8     readQUInt8 ()    { return m_incomingBuffer->readQUInt8();  }
    inline quint8     readQUInt16()    { return m_incomingBuffer->readQUInt16(); }
    inline quint32    readQUInt32()    { return m_incomingBuffer->readQUInt32(); }
    inline qreal      readQReal()      { return m_incomingBuffer->readQReal();   }
    inline QString    readQString()    { return m_incomingBuffer->readQString(); }
    inline QByteArray readBytes(int count) { return m_incomingBuffer->readBytes(count); }
    inline QByteArray readBlock()      { return m_incomingBuffer->readBlock(); }
    inline void readNamedBlock(QString &fileName, QByteArray &fileData) { return m_incomingBuffer->readNamedBlock(fileName, fileData); }

    inline void writeNetCommand(const quint8 command) { m_outgoingBuffer->writeQUInt8(command); }
    inline void writeQUInt8(const quint8 value)       { m_outgoingBuffer->writeQUInt8(value); }
    inline void writeQUInt16(const quint16 value)     { m_outgoingBuffer->writeQUInt16(value); }
    inline void writeQUInt32(const quint32 value)     { m_outgoingBuffer->writeQUInt32(value); }
    inline void writeQReal(const qreal value)         { m_outgoingBuffer->writeQReal(value); }
    inline void writeQString(const QString value)     { m_outgoingBuffer->writeQString(value); }
    inline void writeBlock(const QByteArray &block)   { m_outgoingBuffer->writeBlock(block); }
    inline void writeBlock(const QByteArray *p_block) { m_outgoingBuffer->writeBlock(p_block); }
    inline void writeNamedBlock(const QString &fileName, const QByteArray &fileData) { return m_outgoingBuffer->writeNamedBlock(fileName, fileData); }

    static const QString serverIdentifier;
    static const QString clientIdentifier;

    void enableDebug(bool enabled);
    void setDumpFilename(const QString &filename);

    void requestSendPackage();
    bool event(QEvent *event);

signals:
    /* Debug&Info: */
    void sendLog(QString);
    void chatMessageAccepted(QString);
    void stateChanged(int newState);
    void processPackage();
    void connected();
    void disconnected();

private slots:
    void protocolConnection();
    void protocolAcception();

    void acceptData();
    void whenError();
    void whenSocketDisconnected();

    void debugSink(QString text);

protected:
    bool readPackage();

    static QEvent::Type EventTypeSendPackageRequest;
    bool m_sendPackagePending;

    void sendPackage(bool force = false);
    bool m_isConnected;
    int m_protocolConnectionBytes;

    int m_expectedBytes;
    QTcpSocket *m_socket;
    NetRole m_mode;

    CExtendedArray *m_incomingBuffer;
    CExtendedArray *m_outgoingBuffer;
    quint32 m_protocolVersion;

    bool m_dumpPackage;
    QString m_dumpFilename;
};

#endif // CNETBASE_H
