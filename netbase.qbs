import qbs 1.0

Product {
    Depends { name: "extendedarray" }
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "network"] }
    name: "netbase"
    type: "dynamiclibrary"

    cpp.includePaths: [
        "../CExtendedArray"
    ]

    Group {
        name: "files"
        files: [
            "CNetBase.cpp",
            "CNetBase.hpp"
        ]
    }
}
